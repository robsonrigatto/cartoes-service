package br.com.rr.mastertech.cartoes.dto.request;

import lombok.Data;

@Data
public class UpdateCartaoDTO {

    private Boolean ativo;
}
