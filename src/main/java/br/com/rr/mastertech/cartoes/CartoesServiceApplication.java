package br.com.rr.mastertech.cartoes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CartoesServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CartoesServiceApplication.class, args);
	}

}
