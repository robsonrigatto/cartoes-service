package br.com.rr.mastertech.cartoes.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ClienteDTO {

    private Integer id;
    private String name;
}
