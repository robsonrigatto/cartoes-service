package br.com.rr.mastertech.cartoes.dto.request;

import lombok.Data;

@Data
public class CreateClienteDTO {

    private String name;
}
